#include<stdio.h>

// question one 
void test(){
	char x, y;
	do{
		x = getchar();
		if(x != '\n'){
			y = (char)((x+2) % ('z'+1));  // 保证不出界 
			if(y < 'a')
				putchar(y+'a');
			else putchar(y);
		}
	}while(x != '\n');
}

// question two

int copyfile(const char **argv){
	FILE *fp1 = fopen(argv[1], "r");
	FILE *fp2 = fopen(argv[2], "w");
	if(fp1 == NULL || fp2 == NULL)
		return 0;
	do{
		char x1 = fgetc(fp1);
		if(feof(fp1))  // 文件末尾 
			return 1;
		fputc(x1, fp2);
	}while(1);
}

int main(int argc, const char **argv){
	test();
	copyfile(argv);
	return 0;
}
