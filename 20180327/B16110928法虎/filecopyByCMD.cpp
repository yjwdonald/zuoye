#include <stdio.h>

int main(int argc, char **argv)
{
    FILE* fp1,*fp2;
    char ch;
    char *srcfile=argv[1];
    char *dstfile=argv[2];
    if(argc==3)
    {
        fp1=fopen(srcfile,"r");
        if(!fp1)
        {
            printf("ERROR:file not exit");
            return -1;
        }
        fp2=fopen(dstfile,"w");
	if(!fp2)
	{
	    printf("dstfile open ERROR");
        }

        ch=fgetc(fp1);
        while(!feof(fp1))
        {
            fputc(ch,fp2);
            ch=fgetc(fp1);
        }

        fclose(fp1);
        fclose(fp2);
    }
    return 0;
}
