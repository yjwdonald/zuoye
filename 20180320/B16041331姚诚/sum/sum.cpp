#include<iostream>
using namespace std;

int nsum1(unsigned int n){
	int sum=0;
	while(n){
		sum+=n%10;
		n=n/10;
	}
	return sum;
}

int nsum2(unsigned int n){
	int sum=0;
	do{
		sum+=n%10;
		n=n/10;
	}while(n);
	return sum;
}

int nsum3(unsigned int n){
	int sum=0;
	for(;n;n=n/10){
		sum+=n%10;
	}
	return sum;
}

int nsum4(unsigned int n){
	int sum=0;
	while(1){
		sum+=n%10;
		n=n/10;
		if(!n)break;
	}
	return sum;
}


int main(){
	unsigned int d;
	cout<<"input:";
	cin>>d;
	int s=0;
	for(unsigned int i=1;i<=d;i++)s+=nsum1(i);
	cout<<"sum1:"<<s<<endl;
	 s=0;
	for(unsigned int i=1;i<=d;i++)s+=nsum2(i);
	cout<<"sum2:"<<s<<endl;
	 s=0;
	for(unsigned int i=1;i<=d;i++)s+=nsum3(i);
	cout<<"sum3:"<<s<<endl;
	 s=0;
	for(unsigned int i=1;i<=d;i++)s+=nsum4(i);
	cout<<"sum4:"<<s<<endl;
	return 0;
}