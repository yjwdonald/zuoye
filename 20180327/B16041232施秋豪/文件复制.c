#include<stdio.h>
#include<stdlib.h>
int main(int argc,char **argv)
{
	FILE *fp1;
	FILE *fp2;
	fp1=fopen(argv[1],"r");
	fp2=fopen(argv[2],"w");
	if(fp1==NULL || fp2==NULL)
	{
		perror("ERROR!");
		return -1;
	}
	char ch;
	while((ch=fgetc(fp1))!=EOF)
	{
		fputc(ch,fp2);
	}
	fclose(fp1);
	fclose(fp2);
	return 0;
 } 
