#include<stdio.h>
int Sum1(int n)
{
	int i,sum=0,x;
	for(i=n;i>0;i--)
	{
		x=i;
		for(;x>0;x=x/10)
			sum+=x%10;
	}
	return sum;
}

int Sum2(int n)
{
	int i,sum=0,x;
	i=n;
	while(i>0)
	{
		x=i;
		while(x>0)
		{
			sum+=x%10;
			x=x/10;
		}
		i--;
	}
	return sum;
}

int Sum3(int n)
{
	int i,sum=0,x;
	i=n;
	do
	{
		x=i;
		do
		{
			sum+=x%10;
			x=x/10;
		}while(x>0);
		i--;
	}while(i>0);
	return sum;
}

int Sum4(int n)
{
	int i,sum=0,x;
	i=n;
	while(1)
	{
		if(i<=0)
			break;
		else
		{
			x=i;
			while(1)
			{
				if(x<=0)
					break;
				else
				{
					sum+=x%10;
					x=x/10;
				}
			}
			i--;
		}
	}
	return sum;
}

int main()
{
	int sum,x;
	printf("输入一个多位数：");
	scanf("%d",&x);
	sum=Sum1(x);
	printf("sum1=%d\n",sum);
	sum=Sum2(x);
	printf("sum2=%d\n",sum);
	sum=Sum3(x);
	printf("sum3=%d\n",sum);
	sum=Sum4(x);
	printf("sum4=%d\n",sum);
	return 0;
}
