#include<stdio.h>
#include<stdlib.h>
int main( int argc, char **argv)
{
    FILE *fp1;
    FILE *fp2;
    char *srcfile=argv[1];
    char *destfile=argv[2];
    fp1 = fopen(srcfile,"r");
    if(fp1==0)
    {
        perror("FILE COPY ERROR!\n");
        return -1;
    }
    fp2 = fopen(destfile,"w");
    int ch;
    while((ch=fgetc(fp1))!=EOF)
    {
        if(feof(fp1))
          break;
        fputc(ch,fp2);
    }
   fclose(fp1);
   fclose(fp2);
   return 0;
}
