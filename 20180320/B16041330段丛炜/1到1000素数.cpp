#include<iostream>
using namespace std;

int Is(int n){
	for(int i = 2;i <= n/2;i++){
		if(n%i == 0){
			return 0;
		}
	}
	return 1;
}

int main(){
	int count = 0;
	for(int i = 2;i <= 1000;i++){
		if(Is(i)){
			cout<<i<<" ";
			count ++;
			if(count%5!=0)
				continue;
			cout<<endl;
		} 
		
	} 
}
