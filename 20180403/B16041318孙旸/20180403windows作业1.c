#include<stdio.h>
#include<stdlib.h>
void lmov(char *a)
{
	int i=0;
	while(a[i]!='\0')
	{
		if(a[i]<='Z')
			a[i]=(a[i]-'A'+2)%26+'A';
		else
			a[i]=(a[i]-'a'+2)%26+'a';
		i++;
	}
}

void swap(char *a)
{
	int i=0,j=0;
	char temp;
	while(a[i]!='\0')
	{
		i++;
	}
	while(j<=i/2)
	{
		temp=a[i-1];
		a[i-1]=a[j];
		a[j]=temp;
		j++;
		i--;
	}
}

void change(char *a)
{
	int i=0;
	while(a[i]!='\0')
	{
		if(a[i]<='Z')
			a[i]=a[i]+32;
		else 
			a[i]=a[i]-32;
		i++;	
	}
}

int main(void)
{
	char a[100]=""; 
	gets(a);
	lmov(a);
	swap(a);
	change(a);
	puts(a);
	return 0;
}


