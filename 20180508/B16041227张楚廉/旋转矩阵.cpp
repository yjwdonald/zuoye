#include<stdio.h>
#define N 4
void display(int a[][N])
{
    for(int i=0;i<N;i++)
    {
        for(int j=0;j<N;j++)
        {
            printf("%d",a[i][j]);
            printf(" ");
        }
        printf("\n");
    }
    printf("\n");
}
int main()
{
    int a[N][N]=
	{
        {1,2,3,4},
        {9,10,11,12},
        {13,9,5,1},
        {15,11,7,3} 
	};
	int b[N][N]={0};
	int c[N][N]={0};
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			b[j][3-i]=a[i][j];
			c[3-j][i]=a[i][j];
		}
	}
	printf("原矩阵：\n");
	display(a);
	printf("顺时针旋转后：\n");
    display(b);
	printf("逆时针旋转后:\n");
	display(c);
    return 0;
}
