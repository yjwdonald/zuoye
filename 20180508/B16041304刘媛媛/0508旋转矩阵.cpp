#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int M=0,N=0;
    int a[10][10],b[10][10],c[10][10],d[10][10];
    cout<<"请输入行数：";
    cin>>M;
    cout<<"请输入列数：";
    cin>>N;
    cout<<"请输入一个"<<M<<"行"<<N<<"列"<<"的矩阵："<<endl;
    for(int i=0;i<M;i++)
    {
        for(int j=0;j<N;j++)
        {
            cin>>a[i][j];
        }
    }
    cout<<"原始矩阵为："<<endl;
    for(int i=0;i<M;i++)
    {
        for(int j=0;j<N;j++)
        {
            b[i][j]=a[i][j];
            cout<<setw(5)<<b[i][j];
        }
        cout<<endl;
    }

    cout<<"顺时针旋转矩阵为："<<endl;
    for(int i=0;i<M;i++)
    {
        for(int j=0;j<N;j++)
        {

            c[i][j]=a[N-1-j][i];

            cout<<setw(5)<<c[i][j];
        }
    cout<<endl;
    }

    cout<<"逆时针旋转矩阵为："<<endl;
    for(int i=0;i<M;i++)
    {
        for(int j=0;j<N;j++)
        {

            d[i][j]=a[j][N-1-i];

            cout<<setw(5)<<d[i][j];
        }
    cout<<endl;
    }
  return 0;
}
