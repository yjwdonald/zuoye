#include <iostream>
#include<iomanip>
using namespace std;
#define n 7
int main()
{
    int v = 1;
    int a[n][n];
    for( int i = 0; i < n; i++)
    {
        for(int j = 0; j <= i; j++)
        {
            a[i][j]=v;
            v++;
            a[j][i]=a[i][j];
        }
    }
    for( int i = 0; i < n; i++)
    {
        for( int j = 0; j < n; j++)
        {
            cout<<setw(4)<<a[i][j];
        }
        cout<<endl;
    }
    return 0;
}
